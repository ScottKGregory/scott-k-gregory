import { remToPixels } from "./utils/style";

class State {
  private static instance: State;

  public offWhite: string = "";
  public colorQuaternary: string = "";
  public colorTertiary: string = "";
  public colorSecondary: string = "";
  public colorPrimary: string = "";
  public colorAccent: string = "";

  public canvas: HTMLCanvasElement;
  public renderCanvas: HTMLCanvasElement;

  public detectLetter: boolean = false;
  public allStopped: boolean = false;
  public stopped: number = 0;
  public stoppedTicks: number = 0;

  public font: string = "Delius Unicase";
  public fontSizeRem: number = 25;
  public fontSize: string = `${remToPixels(25)}px`;
  public logoWidth: number = 100;
  public fontWeight: number = 900;
  public words: string[] = ["Welcome", "Croeso", "Välkommen", "Bienvenido", "Velkommen", "Welkom"];
  public logoText: string = "Welcome";

  public bubbleCount: number = 10000;
  public letterThreshold: number = 9900;
  public bubbleMaxSpeed: number = 60;
  public bubbleMinSize: number = 2;
  public bubbleMaxSize: number = 4;

  public dpr: number = 1;

  private constructor() { }

  static getInstance(): State {
    if (!State.instance) {
      State.instance = new State();
    }

    window.addEventListener("DOMContentLoaded", function (event) {
      var state = State.getInstance();
      state.InitColors();

      state.canvas = <HTMLCanvasElement>document.getElementById("background-canvas")!;
      state.renderCanvas = <HTMLCanvasElement>document.getElementById("background-render-canvas")!;

      state.dpr = window.devicePixelRatio || 1;

      state.bubbleMinSize = (state.bubbleMinSize * state.dpr) / 1.5;
      state.bubbleMaxSize = (state.bubbleMaxSize * state.dpr) / 1.5;

      state.fontSize = `${remToPixels(state.fontSizeRem * state.dpr)}px`;

      document.fonts.load(`${state.fontSize} ${state.font}`).then(() => {
        window.dispatchEvent(new Event("StateReady"));
      });
    });

    return State.instance;
  }

  public InitColors() {
    var style = getComputedStyle(document.documentElement);
    this.offWhite = style.getPropertyValue("--off-white");
    this.colorQuaternary = style.getPropertyValue("--color-quaternary");
    this.colorTertiary = style.getPropertyValue("--color-tertiary");
    this.colorSecondary = style.getPropertyValue("--color-secondary");
    this.colorPrimary = style.getPropertyValue("--color-primary");
    this.colorAccent = style.getPropertyValue("--color-accent");
  }
}

export default State;
