import { randomInt } from "../utils/numbers";
import State from "../state";

const state = State.getInstance();

class Bubble {
  maxX: number;
  maxY: number;
  speedX: number;
  speedY: number;
  directionX: boolean;
  directionY: boolean;
  size: number;
  x: number;
  y: number;
  color: string;
  stop: boolean;
  stopIn: { [key: string]: { stopped: number, color: string } };
  fadeOut: boolean;
  firstFade: boolean;
  delete: boolean;
  sizeMod: number;
  initialSpeedY: number;
  initialSpeedX: number;
  maxSpeed: number;
  stoppedColor: string;
  bounce: boolean;

  constructor(
    maxSpeed: number,
    maxX: number,
    maxY: number,
    minSize: number,
    maxSize: number,
    stopIn: { [key: string]: { stopped: number, color: string } },
    color: string,
    stoppedColor: string
  ) {
    this.maxX = maxX;
    this.maxY = maxY;
    this.maxSpeed = maxSpeed;
    this.speedX = randomInt(1, maxSpeed / 10);
    this.speedY = randomInt(1, maxSpeed / 10);
    this.initialSpeedX = this.speedX;
    this.initialSpeedY = this.speedY;
    this.directionX = randomInt(1, 2) == 1;
    this.directionY = randomInt(1, 2) == 1;
    this.size = randomInt(minSize, maxSize);
    this.x = randomInt(0 + this.size, maxX - this.size);
    this.y = randomInt(0 + this.size, maxY - this.size);
    this.color = color;
    this.stoppedColor = stoppedColor;
    this.stop = false;
    this.stopIn = stopIn;
    this.fadeOut = false;
    this.firstFade = true;
    this.delete = false;
    this.sizeMod = 0.4;
    this.bounce = true;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.size > 0 ? this.size : 0, 0, 2 * Math.PI);

    if (!this.stop) {
      if (
        (this.x >= this.maxX - this.size || this.x <= 0 + this.size) &&
        this.bounce
      ) {
        this.directionX = !this.directionX;
        if (state.detectLetter) {
          this.speedX += 0.1;
          this.speedY += 0.1;
        }
      }

      if (
        (this.y >= this.maxY - this.size || this.y <= 0 + this.size) &&
        this.bounce
      ) {
        this.directionY = !this.directionY;
      }

      if (this.directionX) {
        this.x -= this.speedX;
      } else {
        this.x += this.speedX;
      }
      if (this.directionY) {
        this.y -= this.speedY;
      } else {
        this.y += this.speedY;
      }
      ctx.moveTo(this.x, this.y);
      var slow =
        this.stopIn[`${Math.floor(this.x)}|${Math.floor(this.y)}`] != undefined &&
        this.stopIn[`${Math.floor(this.x)}|${Math.floor(this.y)}`].stopped == 0 &&
        randomInt(0, 100) > 60 &&
        state.detectLetter;
      if (slow) {
        this.speedX -= this.maxSpeed / 50;
        this.speedY -= this.maxSpeed / 50;
        if (this.speedX <= 0 && this.speedY <= 0 && this.firstFade) {
          this.color = this.stoppedColor || this.stopIn[`${Math.floor(this.x)}|${Math.floor(this.y)}`].color;
          this.stop = true;
          state.stopped += 1;
          this.firstFade = false;
        }
      } else {
        this.speedX += this.maxSpeed / 100;
        this.speedY += this.maxSpeed / 100;

        this.speedX =
          this.speedX >= this.initialSpeedX ? this.initialSpeedX : this.speedX;
        this.speedY =
          this.speedY >= this.initialSpeedY ? this.initialSpeedY : this.speedY;
      }
    }

    ctx.fill();
  }
}

export default Bubble;
